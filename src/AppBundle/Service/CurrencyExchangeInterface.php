<?php
namespace AppBundle\Service;
/**
 * Interface CurrencyExchangeInterface
 * @package AppBundle\Service
 */

Interface CurrencyExchangeInterface{

    public function __construct($access_key, $api_url, HttpInterface $http );
    public function getLiveCurrencyRates($currencies, $format, $source);

}


?>