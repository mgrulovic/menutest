<?php
namespace AppBundle\Service;
/**
 * Interface HttpInterface
 * @package AppBundle\Service
 */

interface HttpInterface{
    public function performRequest($url, $method, $data= false);
}


?>