<?php
namespace AppBundle\Service;

/**
 * Class CurrencyExchangeFactory
 * @package AppBundle\Service
 */
class CurrencyExchangeFactory
{

    public static function create(CurrencyExchangeInterface $currencyExchangeService)
    {
        return $currencyExchangeService;
    }

}


?>