<?php
namespace AppBundle\Service;

/**
 * Class Http
 * @package AppBundle\Service
 */
class Http implements HttpInterface
{

    protected $curl;

    /**
     * Http constructor.
     */
    public function __construct()
    {
        $this->curl = curl_init();
    }

    /**
     * @param $data
     * @return string
     */
    public function setData($data)
    {
        $params = '';
        $connector = '';
        foreach ($data as $key => $param) {
            $params .= $connector . $key . '=' . $param;
            $connector = '&';
        }
        return $params;
    }

    /**
     * @param $url
     * @param $method
     * @param bool $data
     * @return mixed
     */
    public function performRequest($url, $method, $data = false)
    {
        switch ($method) {
            case "POST":
                curl_setopt($this->curl, CURLOPT_POST, 1);

                if ($data)
                    curl_setopt($this->curl, CURLOPT_POSTFIELDS, $data);
                break;
            case "PUT":
                curl_setopt($this->curl, CURLOPT_PUT, 1);
                break;
            default:
                if ($data) {
                    $data = $this->setData($data);
                    $url = sprintf("%s?%s", $url, $data);
                }

        }
        curl_setopt($this->curl, CURLOPT_URL, $url);
        curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($this->curl);
        curl_close($this->curl);
        return $result;
    }

}


?>