<?php
namespace AppBundle\Service;

/**
 * Class CurrencyExchangeService
 * @package AppBundle\Service
 */
class CurrencyExchangeService implements  CurrencyExchangeInterface {

    protected $access_key;
    protected $api_url;
    protected $http;

    /**
     * CurrencyExchangeService constructor.
     * @param $access_key
     * @param $api_url
     * @param HttpInterface $http
     * set parameters from service configuration
     */
    public function __construct($access_key, $api_url, HttpInterface $http )
    {
        $this->access_key = $access_key;
        $this->api_url = $api_url;
        $this->http = $http;

    }

    /**
     * @param $currencies
     * @return mixed
     */
    public function getLiveCurrencyRates($currencies, $format , $source){
        $result = $this->http->performRequest( $this->api_url.'live', 'GET',['access_key'=> $this->access_key, 'currencies' => $currencies, 'format'=>$format, 'source'=>$source]);
        return $result;

    }

}


?>