(function() {
    /**
     *
     * @param currency
     * @param amount
     * @param currency_amount_holder
     * @param btn
     * @constructor
     */
    function CurrencyExchange (currency,amount, currency_amount_holder,btn) {
        this.currency = currency;
        this.amount = amount;
        this.currency_amount_holder = currency_amount_holder ;
        this.btn = btn;
        this.init();
    }

    /**
     * Add event Listeners
     */
    CurrencyExchange.prototype.init = function(){
        $(this.btn).attr('disabled', true);
        $( this.currency ).on( "change", this.getCurrencyFromId.bind(this));
        $( this.amount ).on( "change", this.getCurrencyFromId.bind(this));
    };

    /**
     * Calculates amount that needs to be payed without discount
     * @param amount
     * @param currency_value
     * @param surcharge
     * @returns {number}
     */
    CurrencyExchange.prototype.calculate = function(amount,currency_value,surcharge){
        var sum = amount * currency_value;
        sum+= sum*surcharge/100;
        return sum;
    };

    /**
     * AJAX call that gets currency data
     */
   CurrencyExchange.prototype.getCurrencyFromId = function() {
       var self = this;
       var amount = $(this.amount).val();
        if(amount!= ''){
            var url = Routing.generate('get_currency_by_id', {
                'id': $(this.currency).val()
            });
            $.ajax({
                url: url,
                type: "get",
                success: function (response) {
                    var sum = self.calculate(amount, response.data.currency_value,response.data.surcharge/100);
                    $(self.currency_amount_holder).html('The amount you have to pay is '+sum+' USD');
                    $(self.currency_amount_holder).removeClass('hidden');
                    $(self.btn).removeAttr("disabled");
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(textStatus, errorThrown);
                }


            });

        }


   };

   new CurrencyExchange( $('#currency_currency'),$('#currency_currencyAmount'),$('#currency_amount_holder'), $('#currency_save') );
})();