<?php
/**
 * console command for adding currency entries in database
 */
namespace AppBundle\Command;

use AppBundle\Entity\Currency;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CreateCurrencyCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('app:create-currencies')

            // the short description shown while running "php bin/console list"
            ->setDescription('Creates new currencies.')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp("This command allows you to create currencies...")
        ;
    }

    /**
     * Persist data to a database
     *
     * @param $name
     * @param int $discount
     * @param null $currencyValue
     * @param null $surcharge
     */

    protected function addCurrency($name,$discount = 0, $currencyValue = null, $surcharge = null){
        $currency = new Currency();
        $currency->setName($name);
        $currency->setCurrencyValue($currencyValue);
        $currency->setSurcharge($surcharge);
        $currency->setDiscount($discount);
        $em = $this->getContainer()->get('doctrine')->getManager();
        $em->persist($currency);
        $em->flush();
    }

    /**
     *
     * add default currencies
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->addCurrency('JPY');
        $output->writeln('You successfully added JPY currency!');
        $this->addCurrency('GBP');
        $output->writeln('You successfully added GBP currency!');
        $this->addCurrency('EUR',2);
        $output->writeln('You successfully added EUR currency!');


    }
}