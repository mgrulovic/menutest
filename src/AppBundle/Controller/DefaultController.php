<?php

namespace AppBundle\Controller;

use AppBundle\Entity\CurrencyPurchased;
use AppBundle\Form\CurrencyType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        $currencyPurchased = new CurrencyPurchased();
        $form = $this->createForm(CurrencyType::class, $currencyPurchased);
        $form->handleRequest($request);
        if($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($currencyPurchased);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'You successfully both currency.');

        }

        return $this->render('AppBundle:default:index.html.twig', array(
            'form' => $form->createView(),
        ));

    }

    /**
     * @Route("/update-currencies", name="update_currencies")
     */

    public function updateCurrenciesAction(){
        $currencyExchange = $this->get('currency_exchange_factory')->getLiveCurrencyRates('JPY,GBP,EUR', 1 , 'USD');
        $currencyExchange =  json_decode($currencyExchange);
        if($currencyExchange->success){
            $em = $this->getDoctrine()->getManager();
            foreach($currencyExchange->quotes as $key => $currencyItem){
                $name = explode ($currencyExchange->source,$key);
                $currency = $em->getRepository('AppBundle:Currency')
                    ->findOneByName($name[1]);
                if($currency){
                    $currency->setCurrencyValue($currencyItem);
                    $em->persist($currency);
                }

            }
            $em->flush();
        }
        return $this->render('AppBundle:default:currency_success.html.twig');

    }
}
