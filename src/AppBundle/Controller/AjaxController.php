<?php

namespace AppBundle\Controller;

use AppBundle\Entity\CurrencyPurchased;
use AppBundle\Form\CurrencyType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class AjaxController extends Controller
{

    /**
     *
     * @param $id
     * @return JsonResponse
     */
    public function getCurrencyByIdAction($id)
    {
        try {
            $em = $this->getDoctrine()->getManager();
            $currency = $em->getRepository('AppBundle:Currency')
                ->findCurrency($id);
            return new JsonResponse([
                'success' => true,
                'code'    => 200,
                'data'    => $currency[0]
            ]);

        } catch (\Exception $exception) {
            return new JsonResponse([
                'success' => false,
                'code' => $exception->getCode(),
                'message' => $exception->getMessage(),
            ]);
        }

    }
}
