<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="currency")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\CurrencyRepository")
 */
class Currency{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @var int
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=50, name="name")
     *
     * @var string
     */
    protected $name;

    /**
     * @ORM\Column(type="float", scale=8, name="currency_value")
     *
     * @var string
     */
    protected $currency_value;

    /**
     * @ORM\Column(type="float", scale=2, name="surcharge")
     *
     * @var string
     */
    protected $surcharge;

    /**
     * @ORM\Column(type="float", scale=2, name="discount")
     *
     * @var string
     */
    protected $discount;
    /**
     * @ORM\OneToMany(targetEntity="CurrencyPurchased", mappedBy="currency")
     */
    private $currenciesPurchased;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Currency
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set currencyValue
     *
     * @param string $currencyValue
     *
     * @return Currency
     */
    public function setCurrencyValue($currencyValue = false)
    {
        if(!$currencyValue){
            switch ($this->getName()){
                case 'JPY':
                    $currencyValue = 107.17;
                    break;
                case 'GBP':
                    $currencyValue = 0.711178;
                    break;
                case 'EUR':
                    $currencyValue = 0.884872;
            }
        }
        $this->currency_value = $currencyValue;

        return $this;
    }

    /**
     * Get currencyValue
     *
     * @return string
     */
    public function getCurrencyValue()
    {
        return $this->currency_value;
    }

    /**
     * Set surcharge
     *
     * @param string $surcharge
     *
     * @return Currency
     */
    public function setSurcharge($surcharge = false)
    {
        switch ($this->getName()){
            case 'JPY':
                $surcharge = 7.5;
                break;
            case 'GBP':
                $surcharge = 5;
                break;
            case 'EUR':
                $surcharge = 5;
        }
        $this->surcharge = $surcharge;

        return $this;
    }

    /**
     * Get surcharge
     *
     * @return string
     */
    public function getSurcharge()
    {
        return $this->surcharge;
    }

    /**
     * Set discount
     *
     * @param float $discount
     *
     * @return Currency
     */
    public function setDiscount($discount = 0)
    {
        $this->discount = $discount;

        return $this;
    }

    /**
     * Get discount
     *
     * @return float
     */
    public function getDiscount()
    {
        return $this->discount;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->currenciesPurchased = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add currenciesPurchased
     *
     * @param \AppBundle\Entity\CurrencyPurchased $currenciesPurchased
     *
     * @return Currency
     */
    public function addCurrenciesPurchased(\AppBundle\Entity\CurrencyPurchased $currenciesPurchased)
    {
        $this->currenciesPurchased[] = $currenciesPurchased;

        return $this;
    }

    /**
     * Remove currenciesPurchased
     *
     * @param \AppBundle\Entity\CurrencyPurchased $currenciesPurchased
     */
    public function removeCurrenciesPurchased(\AppBundle\Entity\CurrencyPurchased $currenciesPurchased)
    {
        $this->currenciesPurchased->removeElement($currenciesPurchased);
    }

    /**
     * Get currenciesPurchased
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCurrenciesPurchased()
    {
        return $this->currenciesPurchased;
    }
}
