<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity
 * @ORM\Table(name="currency_purchased")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\CurrencyPurchasedRepository")
 */
class CurrencyPurchased{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @var int
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Currency", inversedBy="currenciesPurchased")
     * @ORM\JoinColumn(name="currency_id", referencedColumnName="id")
     */
    private $currency;

    /**
     * @ORM\Column(type="float", scale=5, name="exchange_rate")
     *
     * @var float
     */
    protected $exchangeRate;

    /**
     * @ORM\Column(type="float", scale=5, name="surcharge_percentage")
     *
     * @var float
     */
    protected $surchargePercentage;

    /**
     * @ORM\Column(type="float", scale=5, name="surcharge_amount")
     *
     * @var float
     */
    protected $surchargeAmount;

    /**
     * @ORM\Column(type="float", scale=5, name="currency_amount")
     *
     * @var float
     */
    protected $currencyAmount;
    /**
     * @ORM\Column(type="float", scale=5, name="payed_amount")
     *
     * @var float
     */
    protected $payedAmount;

    /**
     * @ORM\Column(type="float", scale=5, name="discount_percentage")
     *
     * @var float
     */
    protected $discountPercentage;

    /**
     * @ORM\Column(type="float", scale=5, name="discount_amount")
     *
     * @var float
     */
    protected $discountAmount;

    /**
     * @ORM\Column(type="datetime", name="date_created")
     *
     * @var datetime
     */
    protected $dateCreated;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set exchangeRate
     *
     * @param float $exchangeRate
     *
     * @return CurrencyPurchased
     */
    public function setExchangeRate($exchangeRate)
    {
        $this->exchangeRate = $exchangeRate;

        return $this;
    }

    /**
     * Get exchangeRate
     *
     * @return float
     */
    public function getExchangeRate()
    {
        return $this->exchangeRate;
    }

    /**
     * Set surchargePercentage
     *
     * @param float $surchargePercentage
     *
     * @return CurrencyPurchased
     */
    public function setSurchargePercentage($surchargePercentage)
    {
        $this->surchargePercentage = $surchargePercentage;

        return $this;
    }

    /**
     * Get surchargePercentage
     *
     * @return float
     */
    public function getSurchargePercentage()
    {
        return $this->surchargePercentage;
    }

    /**
     * Set surchargeAmount
     *
     * @param float $surchargeAmount
     *
     * @return CurrencyPurchased
     */
    public function setSurchargeAmount($surchargeAmount)
    {
        $this->surchargeAmount = $surchargeAmount;

        return $this;
    }

    /**
     * Get surchargeAmount
     *
     * @return float
     */
    public function getSurchargeAmount()
    {
        return $this->surchargeAmount;
    }

    /**
     * Set currencyAmount
     *
     * @param float $currencyAmount
     *
     * @return CurrencyPurchased
     */
    public function setCurrencyAmount($currencyAmount)
    {
        $this->currencyAmount = $currencyAmount;

        return $this;
    }

    /**
     * Get currencyAmount
     *
     * @return float
     */
    public function getCurrencyAmount()
    {
        return $this->currencyAmount;
    }

    /**
     * Set payedAmount
     *
     * @param float $payedAmount
     *
     * @return CurrencyPurchased
     */
    public function setPayedAmount($payedAmount)
    {
        $this->payedAmount = $payedAmount;

        return $this;
    }

    /**
     * Get payedAmount
     *
     * @return float
     */
    public function getPayedAmount()
    {
        return $this->payedAmount;
    }

    /**
     * Set discountPercentage
     *
     * @param float $discountPercentage
     *
     * @return CurrencyPurchased
     */
    public function setDiscountPercentage($discountPercentage)
    {
        $this->discountPercentage = $discountPercentage;

        return $this;
    }

    /**
     * Get discountPercentage
     *
     * @return float
     */
    public function getDiscountPercentage()
    {
        return $this->discountPercentage;
    }

    /**
     * Set discountAmount
     *
     * @param float $discountAmount
     *
     * @return CurrencyPurchased
     */
    public function setDiscountAmount($discountAmount)
    {
        $this->discountAmount = $discountAmount;

        return $this;
    }

    /**
     * Get discountAmount
     *
     * @return float
     */
    public function getDiscountAmount()
    {
        return $this->discountAmount;
    }

    /**
     * Set dateCreated
     *
     * @param \DateTime $dateCreated
     *
     * @return CurrencyPurchased
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated
     *
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * Set currency
     *
     * @param \AppBundle\Entity\Currency $currency
     *
     * @return CurrencyPurchased
     */
    public function setCurrency(\AppBundle\Entity\Currency $currency = null)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * Get currency
     *
     * @return \AppBundle\Entity\Currency
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @ORM\PrePersist
     * Set data before push it to database
     */
    public function populateData()
    {
        $this->setExchangeRate($this->getCurrency()->getCurrencyValue());
        $this->setSurchargePercentage($this->getCurrency()->getSurcharge());
        $this->setSurchargeAmount($this->getCurrencyAmount() * $this->getCurrency()->getCurrencyValue() * $this->getCurrency()->getSurcharge()/100);
        $this->setDiscountPercentage(0);
        $this->setDiscountAmount(0);
        $this->setDateCreated(new \DateTime());
        $this->setPayedAmount($this->getCurrencyAmount() * $this->getExchangeRate() + $this->getSurchargeAmount());
        if($this->getCurrency()->getName() == 'EUR'){
            $discount = $this->getCurrency()->getDiscount();
            $discountAmount = $this->getCurrencyAmount()* $this->getCurrency()->getCurrencyValue() * $this->getCurrency()->getDiscount() / 100;
            $this->setPayedAmount($this->getPayedAmount() - $discountAmount);
            $this->setDiscountPercentage($discount);
            $this->setDiscountAmount($discountAmount);
        }
    }

}
