<?php

namespace AppBundle\Entity\Repository;

/**
 * CurrencyRepository
 *
 */
class CurrencyRepository extends \Doctrine\ORM\EntityRepository
{
    public function findAllOrderedByName()
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT c FROM AppBundle:Currency c ORDER BY c.name ASC'
            )
            ->getResult();
    }
    public function findCurrency($id)
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT c FROM AppBundle:Currency c WHERE c.id='.$id
            )
            ->getArrayResult();
    }

}
