<?php
namespace AppBundle\EventListener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use AppBundle\Entity\CurrencyPurchased;

class SendEmail
{
    protected $mailer;

    /**
     * SendEmail constructor.
     * @param \Swift_Mailer $mailer
     */
    public function __construct(\Swift_Mailer $mailer){
        $this->mailer = $mailer;
    }

    /**
     * if GBP currency is purchased send email notification
     * @param LifecycleEventArgs $args
     */
    public function postPersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        if (!$entity instanceof CurrencyPurchased) {
            return;
        }

        if($entity->getCurrency()->getName() == 'GBP'){
            $message = \Swift_Message::newInstance()
                ->setSubject('GBP currency')
                ->setBody('GBP currency is purchased on your website.')
            ;
            $this->mailer->send($message);
        }
    }
}