README


1. Installing the project: 
    to install symfony3 vendor files open cmd and run php composer.phar update 



2. Database: 
    2.1. php bin/console doctrine:database:create
    2.2. php bin/console doctrine:schema:update --force
    2.3. php bin/console app:create-currencies
    
    
3. clear the cashe :
    php bin/console cache:clear --env=prod
    
    
4. In C:\xampp\htdocs\menutest\app\config\config.yml part swiftmailer you can change email address where email should be sent
    
    
Using the project:

    1. Homepage  - page where you can buy currencies :  
        You have to choose both currency and amount so the calculated amount without the discount 
        can be shown and also button would be enabled . Logic is stored in :
        C:\xampp\htdocs\currencyExchange\src\AppBundle\Resources\public\js\currency.js file
        After populating the form AJAX is going to be called and pick currency data so amount of USD can be calculated.
        On submit pre persist action is going to be called so all data can be stored and discount is added if needed.
        On post persist if purchased currency is GBP symfony event listener is going to catch event and send email.
        
    2. update-currencies - page for updating currency in database  
        updateCurrenciesAction in DefaultController is calling factory that returns service , that is going to do api call, with sent parameters,
        after the response is retuirned data are updated in database
          

